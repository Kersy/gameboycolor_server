package com.laconic.emulator;

import java.net.Socket;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.zip.Deflater;

public class VirtualMachine extends Thread {

  private Emulator emulator = null;
  private Socket socket = null;
  protected BufferedOutputStream output = null;
  private Deflater compressor = new Deflater();
  // ByteBuffer buffer = ByteBuffer.allocate(23040);
  // private double id = 0.0;

  ByteBuffer buffer = ByteBuffer.allocate(23040);
  long startTime = 0;
  long endTime = 0;
  long cycle = 0;
  long cycleTime = 1000000000; // billion
  int hz = 60;
  // int ran = (new Random()).nextInt(100);

  @Override
  public void run() {
    if (socket != null) {
      try {
        this.output = new BufferedOutputStream(socket.getOutputStream());
//        this.output = new GZIPOutputStream(socket.getOutputStream());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    step();
  }

  public VirtualMachine(Emulator emulator, Socket socket) {
    super("VirtualMachine");
    try {
      this.emulator = emulator;
      this.socket = socket;
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public VirtualMachine(Emulator emulator) {
    this.emulator = emulator;
    System.out.println("initialized");
  }

  public void loop() { }

  public void step() {
//    long startTime = 0;
    int MAX_CYCLES = 69905;
    long cycles = 0;
    int counter = 0;

    // do a cycle every 1/60th of a second
    while (true) {

      startTime = System.nanoTime();
      try{
        emulator.decode();
        cycles += emulator.getCurrentCycles();

        if (cycles >= (MAX_CYCLES)) {
          cycles = 0;
          pause(startTime);
  
          if (counter++ == 6) {
            try {
              // count++;
              buffer = ByteBuffer.allocate(23040);
              byte[] compressedData = new byte[5_000];
              byte[][][] display = emulator.getDisplay();
              for (byte[][] b : display) { // we should always flush the display after the appropriate cycle
                buffer.put(b[0]);
              }
              compressor.setInput(buffer.array());
              compressor.finish();
              short length = (short)compressor.deflate(compressedData);
  
              byte[] len = new byte[2];
              len[0] = (byte)(length >> 8); //higher 8 bits
              len[1] = (byte) length;
  
              this.output.write(len);
              for(int i=0; i<length; i++){
                this.output.write(compressedData[i]);
              }
  
              compressor.reset();
              this.output.flush();
  
            } catch (Exception e) {
              System.out.println("input shut: " + this.socket.isInputShutdown());
              System.out.println("output shut: " + this.socket.isOutputShutdown());
              disconnect();
              System.out.println(e.getMessage());
              break;
            }
            counter = 0;
          }
        }
      }catch(Exception e){
        System.out.println("error while decoding "+e.getMessage());
      }
    }
  }

  private void disconnect() {
    try {
      this.output.close();
      this.socket.close();
      this.output = null;
      this.socket = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void loadROM(String romName){
  }

  public String saveState(){
    return emulator.saveState("");
  }

  public void pause(long start){
    while((System.nanoTime() - start) < (1_000_000 / 60)){ //loop until we reach 1/60th of a second which is
      try{
        Thread.sleep(1);
      }catch(Exception e){
        e.printStackTrace();
      }
    }
  }

  private void sendCompressedStream(byte[] data){

  }

  private void sendStream(byte[] data){

  }


}
