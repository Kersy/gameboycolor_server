package com.laconic.emulator;

import com.laconic.gameboy.GBJoypad;
import com.laconic.gameboy.GBProcessor;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.Socket;

public class Input extends Thread {

    private BufferedInputStream input;
    private GBJoypad joypad;
    private GBProcessor cpu;

    public Input(Socket socket, GBJoypad joypad, GBProcessor cpu) {
        try {
            this.input = new BufferedInputStream(socket.getInputStream());
            this.joypad = joypad;
            this.cpu = cpu;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                while (this.input.available() > 0) {
                    // System.out.println("received data");
                    byte[] key = new byte[2];
                    this.input.read(key, 0, 2);
                    if (key[0] == 49) {
                        mapButtonInput(key[1], true);
                    } else if (key[0] == 48) {
                        mapButtonInput(key[1], false);
                    } else if(key[0] == 108){ //loadrom
                        byte[] l = {key[1]};
                        int length = Integer.valueOf(new String(l));
                        byte[] fileName = new byte[length];
                        this.input.read(fileName,0,fileName.length);
                        String s = new String(fileName);
                        this.cpu.load(s.trim());
                    } else if(key[0] == 109){  //save
                        byte[] l = {key[1]};
                        int length = Integer.valueOf(new String(l));
                        byte[] uname = new byte[length];
                        this.input.read(uname,0,uname.length);
                        String username = new String(uname);

                        this.cpu.saveState(username);
                    } else if(key[0] == 111){ //load
                        byte[] l = {key[1]};
                        int length = Integer.valueOf(new String(l));
                        byte[] uname = new byte[length];
                        this.input.read(uname,0,uname.length);
                        String username = new String(uname);

                        this.cpu.loadState(username);                    
                    } else {
                        System.out.println("unrecognized input: " + key[0]);
                    }
                }
            }
        } catch (Exception e) {
            try {
                this.input.close();
                this.input = null;
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void mapButtonInput(byte key, boolean direction){
        // byte[] k = {key};
        // System.out.println("getting input? "+new String(k));
        if(!direction){
            switch(key){
                case 'l': //left
                    joypad.setDirection((byte)0b1101);
                    break;

                case 't': //right
                    joypad.setDirection((byte)0b1110);
                    break;

                case 'u': //up
                    joypad.setDirection((byte)0b1011);
                    break;

                case 'n': //down
                    joypad.setDirection((byte)0b0111);
                    break;

                case 'a': //A
                    joypad.setButton((byte)0b1110);
                    break;

                case 's': //B
                    joypad.setButton((byte)0b1101);
                    break;

                case 'E': //start
                    joypad.setButton((byte)0b0111);
                    break;

                case 'S': //select
                    joypad.setButton((byte)0b1011);
                    break;
            }
        }else{
            switch(key){
                case 'l': //left
                    joypad.resetDirection((byte)0b1101);
                    break;

                case 't': //right
                    joypad.resetDirection((byte)0b1110);
                    break;

                case 'u': //up
                    joypad.resetDirection((byte)0b1011);
                    break;

                case 'n': //down
                    joypad.resetDirection((byte)0b0111);
                    break;

                case 'a': //A
                    joypad.resetButton((byte)0b1110);
                    break;

                case 's': //B
                    joypad.resetButton((byte)0b1101);
                    break;

                case 'E': //start
                    joypad.resetButton((byte)0b0111);
                    break;

                case 'S': //select
                    joypad.resetButton((byte)0b1011);
                    break;
            }
        }
    }
}
