package com.laconic.emulator;

import java.awt.*;
import javax.swing.*;

import com.laconic.util.Palette;

public class Display extends JPanel{

  /**
   *
   */
  private static final long serialVersionUID = 7229449468813297549L;
  Graphics g;
  byte[][][] display;
  int width;
  int height;
  Color[] colorPicker = new Color[4];
  int multiplier = 1;
  boolean isSprite = false;

  public Display(byte[][][] data, int width, int height,int stretch){
    this.width = width;
    this.height = height;
    this.display = data;
    this.setBorder(BorderFactory.createLineBorder(Color.black));
    this.setFocusable(true);
    this.multiplier = stretch;
  }

  public Dimension getPreferredSize(){
    return new Dimension(width,height);
  }

  public void refreshDisplay(byte[][][] data){
    this.display=data;
    isSprite = false;
    repaint();
  }

  public void refreshDisplay(byte[][][] sprite, Palette p){
    this.display = sprite;
    isSprite = true;
//    this.spritePalette = p;
    repaint();
  }

  public void paintComponent(Graphics g){
    super.paintComponent(g);
    this.g = g;

    for(int i=0; i<width; i++){ //w=160
      for(int j=0; j<height; j++){ //h=144
        paintDisplay(display[j][i][0],display[j][i][1],i,j);
//        paintDisplay(display[i][j],i,j);
      }
    }
  }

  private void paintDisplay(byte color1, byte color2, int x, int y){
    int r = (color1 & 0b00011111) << 3;
    int gr = (((color1 & 0b11100000) >> 5) + ((color2 & 0b11) << 3) << 3);
    int b = (color2 & 0b01111100 >> 2) << 3;
    try{
      g.setColor(new Color(r,gr,b));
      g.fillRect(x*multiplier,y*multiplier,1*multiplier,1*multiplier);
    }catch(Exception e){
      System.out.println("red: "+r+", green: "+gr+", blue: "+b);
    }
  }

}
