package com.laconic.chipeight;

import com.laconic.emulator.EmulatorState;
import com.laconic.emulator.EmulatorType;
import com.laconic.util.SmallStack;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;

public abstract class Chip8State extends EmulatorState{

  int stateCounter = 0;
  static final int MAX_MEM = 4096;
  static final byte X = 64;
  static final byte Y = 32;
  static int[] chars = {
    0xF0, 0x90, 0x90, 0x90, 0xF0, //0
    0x20, 0x60, 0x20, 0x20, 0x70, //1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
    0x90, 0x90, 0xF0, 0x10, 0x10, //4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
    0xF0, 0x10, 0x20, 0x40, 0x40, //7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
    0xF0, 0x90, 0xF0, 0x90, 0x90, //A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
    0xF0, 0x80, 0x80, 0x80, 0xF0, //C
    0xE0, 0x90, 0x90, 0x90, 0xE0, //D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
    0xF0, 0x80, 0xF0, 0x80, 0x80  //F
  };

  public Chip8State(String filename){
    this.type = EmulatorType.CHIP8;
    this.filename = filename;
    this.file = new File(this.path+filename+Integer.toString(this.stateCounter)+generateFileExtension());
    try{
      this.fis = new FileOutputStream(this.file);
      // this.inputstream = new FileInputStream(this.file);
    }catch(Exception e){
      System.out.println("could not successfully create Emulator State");
      e.printStackTrace();
    }
  }

//  @Override
  public boolean save(){
    return false;
  }

  public byte[][] loadDisplay(){
    int len = "display\n".getBytes().length;
    int disLen = 64 * 32;
    byte[][] display = new byte[64][32];
    byte[] head = new byte[len];
    byte[] tempDisplay = new byte[disLen];
    try{
      this.inputstream.read(head);
      this.inputstream.read(tempDisplay);
      for(int i=0; i<64; i++){ //y
        for(int j=0; j<32; j++){ //x
          display[i][j] = tempDisplay[((i*64)/2)+j];
        }
      }
      this.inputstream.read(new byte[1]);
    }catch(Exception e){
      System.out.println("error loading display");
      e.printStackTrace();
    }
    return display;
  }

  public short loadCounter(){
    byte[] header = "counter\n".getBytes();
    byte[] counter = new byte[2];
    try{
      this.inputstream.read(header);
      this.inputstream.read(counter);
      this.inputstream.read(new byte[1]);
    }catch(Exception e){
      System.out.println("error loading counter");
      e.printStackTrace();
    }
    short c = bytesToShort(counter[0],counter[1]);
    System.out.println("counter: "+ c);
    return c;
    // return (short)(((counter[0] & 0xFF) << 8) + (counter[1] & 0xFF));
  }

  public byte[] loadRegisters(){
    byte[] reg = "register\n".getBytes();
    byte[] registers = new byte[16];
    System.out.println("registers");
    try{
      for(int i=0; i<registers.length; i++){
        this.inputstream.read(reg); //reading header
        byte[] r = new byte[1]; //creating byte to read
        this.inputstream.read(r); //reading byte
        registers[i] = r[0]; //assigning byte to byte array
        System.out.println(r[0]);
        this.inputstream.read(new byte[1]); //reading new line
      }
    }catch(Exception e){
      System.out.println("error loading registers");
      e.printStackTrace();
    }
    return registers;
  }

  public EmulatorType getEmuType(){
    return this.type;
  }
  
  public short loadAddressReg(){
    byte[] add = "addressreg\n".getBytes();
    byte[] addressreg = new byte[2];
    try{
      this.inputstream.read(add);
      this.inputstream.read(addressreg);
      this.inputstream.read(new byte[1]);
    }catch(Exception e){
      System.out.println("error loading address register");
      e.printStackTrace();
    }

    short a = bytesToShort(addressreg[0],addressreg[1]);
    System.out.println("address reg: "+a);
    return a;
  }

  public short loadCurrentInstruction(){
    byte[] ins = "instruction\n".getBytes();
    byte[] instruction = new byte[2];
    try{
      this.inputstream.read(ins);
      this.inputstream.read(instruction);
      this.inputstream.read(new byte[1]);
    }catch(Exception e){
      System.out.println("error loading current instruction");
      e.printStackTrace();
    }
    short i = bytesToShort(instruction[0],instruction[1]);
    // System.out.println("instruction: "+i);
    return i;
  }

  public SmallStack loadStack(){
    byte[] st = "stack\n".getBytes();
    byte[] pointer = new byte[1];
    byte[] sz = "size\n".getBytes();
    byte[] size = new byte[1];
    SmallStack stack = new SmallStack(16);
    SmallStack resStack = new SmallStack(16);
    System.out.println("stack");
    try{
      this.inputstream.read(st); //read header
      this.inputstream.read(pointer); //read pointer
      this.inputstream.read(new byte[1]); //newline
      this.inputstream.read(sz); //read size header
      this.inputstream.read(new byte[1]); //don't care about the stack size honestly, needs to be taken out
      this.inputstream.read(new byte[1]); //newline
      // short point = bytesToShort(pointer[0],pointer[1]);
      short point = pointer[0];
      System.out.println("pointer: "+point);
      byte[] p = new byte[2];
      while(point > -1){
        point--;
        this.inputstream.read(p);
        short s = bytesToShort(p[0],p[1]);
        stack.push(s);
        this.inputstream.read(new byte[1]);
      }
      short pointTwo = pointer[0];
      while(pointTwo > -1){ //doing it again... really need to stop
        pointTwo--;
        short s = stack.pop();
        resStack.push(s);
      }
    }catch(Exception e){
      System.out.println("error loading stack");
      e.printStackTrace();
    }
    return resStack;
  }

  public byte loadDelayTimer(){
    byte[] d = "delay\n".getBytes();
    byte[] delay = new byte[1];
    try{
      this.inputstream.read(d);
      this.inputstream.read(delay);
      this.inputstream.read(new byte[1]);
    }catch(Exception e){
      System.out.println("error loading delay timer");
      e.printStackTrace();
    }
    System.out.println("delay: "+delay[0]);
    return delay[0];
  }

  public byte loadSoundTimer(){
    byte[] d = "sound\n".getBytes();
    byte[] delay = new byte[1];
    try{
      this.inputstream.read(d);
      this.inputstream.read(delay);
      this.inputstream.read(new byte[1]);
    }catch(Exception e){
      System.out.println("error loading sound delay timer");
      e.printStackTrace();
    }
    System.out.println("sound: "+delay[0]);
    return delay[0];
  }

  public byte[] loadMemory(){
    byte[] mem = "memory\n".getBytes();
    byte[] memory = new byte[4096];
    try{
      this.inputstream.read(mem);
      this.inputstream.read(memory);
      this.inputstream.read(new byte[1]);
    }catch(Exception e){
      System.out.println("error loading memory");
      e.printStackTrace();
    }
    return memory;
  }

  private short bytesToShort(byte one, byte two){
    return (short)(((one & 0xFF) << 8) + (two & 0xFF));
  }

}
