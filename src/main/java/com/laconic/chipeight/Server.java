package com.laconic.chipeight;

import java.net.Socket;

import java.net.ServerSocket;
import com.laconic.emulator.*;
import com.laconic.gameboy.*;

public class Server{

  public static void main(String args[]){
    try{
      ServerSocket s = new ServerSocket(60008);
      while(true){
        Socket listener = s.accept();
        GBJoypad joypad = new GBJoypad();
        GBProcessor cpu = new GBProcessor(joypad);
        new Input(listener,joypad, cpu).start();
        new VirtualMachine(cpu,listener).start();
      }
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  private static String getUserName(){
    return "admin";
  }

}
