package com.laconic.gameboy;

import com.laconic.util.*;

import java.util.Map;

public class Tile{

  protected final static int ONE = 0;
  protected final static int TWO = 2;
  protected final static int THREE = 4;
  protected final static int FOUR = 6;

  byte[] data;
  byte[] palette;
  Palette p; //just start with default

  public Tile(byte[] data){
    this.data = data;
    this.p = new Palette((byte)0xE4);
  }

  // public Tile(byte[] data, byte b){ //might need this for testing purposes
  //   this.data = data;
  //   this.p = new Palette(b);
  // }

  public Tile(byte[] data, byte[] palette){
    this.data = data;
    this.palette = palette;
  }

  public void setPalette(byte b){
    this.p = new Palette(b);
  }

  public byte[][][] convertRawColor(){
    byte[][][] tmp = new byte[8][8][2];
      for(int i=0; i<16; i+=2){
        byte b1 = data[i];
        byte b2 = data[i+1];
        for(int j=0; j<8; j++){
          int bit1 = (((1 << j) & b1) == 0)? 0 : 1;
          int bit2 = (((1 << j) & b2) == 0)? 0 : 1;
          if(bit1 > bit2){ //1 & 0
            tmp[(i/2)][7-j][0] = palette[TWO];
            tmp[(i/2)][7-j][1] = palette[TWO+1];
          }else if(bit2 > bit1){
            tmp[(i/2)][7-j][0] = palette[THREE];
            tmp[(i/2)][7-j][1] = palette[THREE+1];
          }else if(bit1 + bit2 == 0){
            tmp[(i/2)][7-j][0] = palette[ONE];
            tmp[(i/2)][7-j][1] = palette[ONE+1];
          }else if(bit1 + bit2 == 2){
              tmp[(i/2)][7-j][0] = palette[FOUR];
              tmp[(i/2)][7-j][1] = palette[FOUR+1];
          }else{
            System.out.println("error");
          }
        }
      }
    return tmp;
  }

  public byte[][] convertRawData(){
    byte[][] tmp = new byte[8][8];
    for(int i=0; i<data.length; i+=2){
      tmp[(i/2)] = bytesToLine(data[i],data[i+1]);
    }
    return tmp;
  }

  public byte[][][] flipX(byte[][][] sprite){
    byte[][][] result = new byte[data.length/2][8][2];
    for(int i=0; i<data.length/2; i++){
      for(int j=0; j<8; j++){
        result[i][j][0] = sprite[i][7-j][0];
        result[i][j][1] = sprite[i][7-j][1];
      }
    }
    return result;
  }

  public byte[][][] flipY(byte[][][] sprite){
    byte[][][] result = new byte[data.length/2][8][2];
    // System.out.println("data length: "+data.length);
    for(int i=0; i<data.length/2; i++){
      for(int j=0; j<8; j++){
        result[i][j][0] = sprite[(data.length/2 - 1) - i][j][0];
        result[i][j][1] = sprite[(data.length/2 - 1) - i][j][1];
      }
    }
    return result;
  }

  public byte[] bytesToLine(byte l1, byte l2){
//    int line1 = l1 & 0xFF;
//    int line2 = l2 & 0xFF;
    byte[] colors = new byte[8];
    int colorSet = -1;
    Map m = p.getNumMap();

    for(int i=0; i<8; i++){
      int bit1 = (((1 << i) & l2) == 0)? 0 : 1;
      int bit2 = (((1 << i) & l1) == 0)? 0 : 1;

      if((bit1 == 1) && (bit2 == 1)){

        colorSet = (byte) m.get((byte)3);

      }else if((bit1 == 1) && (bit2 == 0)){

        colorSet = (byte) m.get((byte)2);

      }else if((bit1 == 0) && (bit2 == 1)){

        colorSet = (byte) m.get((byte)1);

      }else if((bit1 == 0) && (bit2 == 0)){

        colorSet = (byte) m.get((byte)0);

      }else{
        colorSet = -1;
//        System.out.println("error in convert bytes to color -- line1: "+line1+", line2: "+line2);
      }
      colors[7-i] = (byte)colorSet;
    }
    return colors;
  }

  public int getBit(int n, int bitComp){
    int highBit = (1 << bitComp) & (n & 0xFF);
    return highBit >> bitComp;
  }

  public void printFormattedTile(){}

}
