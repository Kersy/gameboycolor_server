package com.laconic.gameboy;

import java.io.FileInputStream;
import java.io.File;
import java.util.*;

public class GBCart{

  public final static int bankOffset = 16000;

  private byte[] ROM = new byte[0x32000];
  private byte[]  RAM = new byte[0x2000];

  private int bankPointer = 0x4000;
  private String model = "";
  private String filename;
  byte[] BOOT = new byte[0x100];

  public enum CartType{
    MBC1;//just this for now
  }

  public void writeRAM(int position, int data){
    try{
      this.RAM[position - 0xA000] = (byte) data;
    }catch(Exception e){
      System.out.println("position: "+String.format("0x%02X",position));
    }
  }

  public byte readRAM(int position){
    return this.RAM[position - 0xA000];
  }

  public GBCart(String url){
    this.filename = url;
    loadCart();
    System.out.println("Cart type: "+String.format("0x%02X",this.ROM[323]));
    // loadBootROM();
  }

  public GBCart(File file){
    loadFile(file);
    bankPointer = 0x4000;
  }

  public void swapBank(int bank){
    if((bank == 0) || (bank == 1)){
      this.bankPointer = 0x4000;
    }else{
      // System.out.println("swapping to bank: "+bank);
      this.bankPointer = 0x4000 * bank;
    }
  }

  public byte readBank(int pos){
    return this.ROM[(bankPointer + (pos - 0x4000))];
  }

  public GBCart(int[] cartData){
    for(int i=0; i<cartData.length; i++){
      this.ROM[i] = (byte)cartData[i];
    }
  }

  public byte[] getLogo(){
    byte[] tile = new byte[48];
    for(int i=0; i<tile.length; i++){
      tile[i] = this.ROM[i];
    }
    return tile;
  }

  private void loadBootROM(){
    int[] boot = Boot.GAMEBOY_CLASSIC;
    for(int i=0; i<0x100; i++){
      BOOT[i] = (byte)boot[i];
    }
    for(int i=0; i<0x100; i++){
      this.ROM[i] = this.BOOT[i];
    }
  }

  public void loadFile(File file){
    try{
      FileInputStream fis = new FileInputStream(file);
      long len = fis.getChannel().size();
      this.ROM = new byte[(int)len];
      fis.read(this.ROM);

      fis.close();
      System.out.println("successfully loaded cart, bytes: "+len);
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  public void loadCart(){
    //should start by checking cart type, but right now we are only using MBC1
    //byte[] data = new byte[32000]; //32kb for now, the size of tetris
    try{
      File file = new File(this.filename);
      // System.out.println("is file readable: "+Files.isReadable(file.toPath()));
      FileInputStream fis = new FileInputStream(file);
      long len = fis.getChannel().size();
      this.ROM = new byte[(int)len];
      fis.read(this.ROM);

      fis.close();
      System.out.println("successfully loaded cart, bytes: "+len);

    }catch(Exception e){
      e.printStackTrace();
    }
    // return data;
  }

  public byte getByte(int address){
    return this.ROM[address];
  }

  public byte[] getBytes(int offSet, int len){
    if(len == 0){
      System.out.println("error transfering data from cart - len zero");
      return null;
    }else if((len + offSet) > 32000){
      System.out.println("error transfering data from cart - attempting to read in more data than availabe in 32kB ROM");
      return null;
    }
    byte[] data = new byte[len];
    int i = 0;
    // System.out.println("length: "+len);
    while(i < len){
      // System.out.println(i);
      data[i] = ROM[i + offSet];
      i++;
    }
    return data;
  }

  public String getModel(){
    return this.model;
  }

}
