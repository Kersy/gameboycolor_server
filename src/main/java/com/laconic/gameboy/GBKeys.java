package com.laconic.gameboy;

public class GBKeys{

  private GBInterruptManager interruptManager;
  private GBMMU mmu;

  public enum Keys{
    RIGHT(0b0000001), LEFT(0b00000010), UP(0b00000100), DOWN(0b00001000),
    A(0b00010000), B(0b0010000000), SELECT(0b01000000), START(0b10000000);

    private int handler;

    Keys(int handler){
      this.handler = handler;
    }

    public int getType(){
      return this.handler;
    }
  }

  public GBKeys(GBInterruptManager interruptManager, GBMMU mmu){
    this.interruptManager = interruptManager;
    this.mmu = mmu;
  }

  public void pressKey(Keys key){
    byte b = (byte)(this.mmu.read(0xFF00) | key.getType());
    this.mmu.write(0xFF00,b);
  }

  public void releaseKey(Keys key){
    byte b = (byte) (this.mmu.read(0xFF00) & ~key.getType());
    this.mmu.write(0xFF00, (byte) b);
  }

}
