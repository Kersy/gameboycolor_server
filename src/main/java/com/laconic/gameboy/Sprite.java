package com.laconic.gameboy;

import com.laconic.util.Palette;

public class Sprite extends Tile{

    Palette p;
    byte[] data;

    public Sprite(byte[] d, byte[] b){
        super(d, b);
        this.data = d;
    }

    public byte[][][] convertRawColor(){
        byte[][][] tmp = new byte[(data.length)/2][8][2];
          for(int i=0; i<data.length; i+=2){
            byte b1 = data[i];
            byte b2 = data[i+1];
            for(int j=0; j<8; j++){
              int bit1 = (((1 << j) & b1) == 0)? 0 : 1;
              int bit2 = (((1 << j) & b2) == 0)? 0 : 1;
              if(bit1 > bit2){ //1 & 0
                tmp[(i/2)][7-j][0] = palette[TWO];
                tmp[(i/2)][7-j][1] = palette[TWO+1];
              }else if(bit2 > bit1){
                tmp[(i/2)][7-j][0] = palette[THREE];
                tmp[(i/2)][7-j][1] = palette[THREE+1];
              }else if(bit1 + bit2 == 0){
                // tmp[(i/2)][7-j][0] = palette[ONE];
                // tmp[(i/2)][7-j][1] = palette[ONE+1];
                tmp[(i/2)][7-j][0] = 4;
                tmp[(i/2)][7-j][1] = 4;
              }else if(bit1 + bit2 == 2){
                tmp[(i/2)][7-j][0] = palette[FOUR];
                tmp[(i/2)][7-j][1] = palette[FOUR+1];
              }else{
                System.out.println("error");
              }
            }
          }
        return tmp;
      }

    // @Override
    // public byte[] bytesToLine(byte l2, byte l1){
    //     byte[] colors = new byte[8];
    //     int colorSet = -1;
    //     Map m = p.getNumMap();

    //     for(int i=0; i<8; i++){
    //         int bit1 = (((1 << i) & l1) == 0)? 0 : 1;
    //         int bit2 = (((1 << i) & l2) == 0)? 0 : 1;

    //         if((bit1 == 1) && (bit2 == 1)){

    //             colorSet = (byte) m.get((byte)3);

    //         }else if((bit1 == 1) && (bit2 == 0)){

    //             colorSet = (byte) m.get((byte)2);

    //         }else if((bit1 == 0) && (bit2 == 1)){

    //             colorSet = (byte) m.get((byte)1);

    //         }else if((bit1 == 0) && (bit2 == 0)){

    //             colorSet = (byte)4;

    //         }else{
    //             colorSet = -1;
    //         }
    //         colors[7-i] = (byte)colorSet;
    //     }
    //     return colors;
    // }
}
