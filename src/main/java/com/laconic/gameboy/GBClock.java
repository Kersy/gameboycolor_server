package com.laconic.gameboy;

public class GBClock{

  private GBProcessor cpu;
  int numOfCycles = 0;

  public GBClock(GBProcessor cpu){
    this.cpu=cpu;
  }

  public void tick(){
    this.cpu.decode();
    this.numOfCycles = this.cpu.getCycleCount();
    System.out.println("number of cycles: "+this.numOfCycles);
  }
}
