package com.laconic.gameboy;

import java.nio.ByteBuffer;
import com.laconic.emulator.StatefulComponent;


public class GBMMU implements StatefulComponent {

  public byte[] specialRegisters = new byte[0x100];

  private int bankNumber = 0;
  private byte[] bankzero = new byte[4096];
  private byte[][] bankN = new byte[7][4096];

  private byte[] internalRAM = new byte[8192];
  private byte[] echoRAM = new byte[8192];
  private byte[] HRAM = new byte[128];
  // private byte[] OAM = new byte[160];

  // private PPU cppu;
  private ColorPPU cppu;
  private GBCart cart;
  private GBJoypad joypad;
  private GBTimer timer;

  // private int bank = 0;

  public GBMMU(GBCart cart, GBJoypad joypad) {
    this.cart = cart;
    this.joypad = joypad;
  }

  // public void setPPU(PPU ppu){ this.cppu = ppu; }
  public void setPPU(ColorPPU cppu) {
    this.cppu = cppu;
  }

  public void setTimer(GBTimer timer){
    this.timer = timer;
  }

    /*
   *
   * MMU WRITE
   *
   */

  public void write(int pos, int data) {
    int position = pos & 0xFFFF;

    if (position == 0x2000 || position == 0x2100) {
      // System.out.println("switch to bank: "+data);
      this.cart.swapBank(data);

    } else if (position < 0x8000) { // CAN'T WRITE HERE

      // System.out.println("error - cannot write to ROM: "+position+", data: "+(data
      // & 0xFF));

    } else if ((position == 0xFF04)) {

      this.specialRegisters[0x04] = (byte) data;

    } else if ((position == 0xFF46)) { // DMA Transfer, needs to be done before writing to other GPU registers (I
                                       // think)

      System.out.println("requesting DMA transfer from: " + data);

    }else if(position == 0xFF70){ 

      if(data == 0){
        this.bankNumber = 0;
      }else{
        this.bankNumber = data - 1;
      }
      // System.out.println("RAM bank change requested: "+data);
      // this.bankNumber = data;

    }else if ((position >= 0xFF40) && (position < 0xFF70)) { // GPU registers

      switch (position) {
      case 0xFF40:
        System.out.println("writing lcdc: "+data);
        this.cppu.LCDC = (byte) data;
        break;

      case 0xFF41:
        this.cppu.STAT = (byte) data;
        break;

      case 0xFF42:
        this.cppu.SCY = (byte) data;
        break;

      case 0xFF43:
        this.cppu.SCX = (byte) data;
        break;

      case 0xFF44:
        this.cppu.LY = (byte) data;
        break;

      case 0xFF45:
        this.cppu.LYC = (byte) data;
        break;

      case 0xFF46:
        this.cppu.DMA = (byte) data;
        break;

      case 0xFF47:
        this.cppu.BGP = (byte) data;
        break;

      case 0xFF48:
        this.cppu.OBP0 = (byte) data;
        break;

      case 0xFF49:
        this.cppu.OBP1 = (byte) data;
        break;

      case 0xFF4A:
        this.cppu.WY = (byte) data;
        break;

      case 0xFF4B:
        this.cppu.WX = (byte) data;
        break;

      case 0xFF68:
        // System.out.println("writing to FF68: "+data);
        this.cppu.BGPI = (byte) data;
        break;

      case 0xFF69:
        // System.out.println("writing to FF6A: "+data);
        this.cppu.writeBGP((byte)data);
        break;

      case 0xFF6A:
        this.cppu.OBPI = (byte) data;
        break;

      case 0xFF6B:
        // System.out.println("writing to FF6B: "+data);
        this.cppu.writeOBJP((byte) data);
        break;

      case 0xFF51:
        this.cppu.SRCH = (byte) data;
        break;

      case 0xFF52:
        this.cppu.SRCL = (byte) data;
        break;

      case 0xFF53:
        this.cppu.DSTH = (byte) data;
        break;

      case 0xFF54:
        this.cppu.DSTL = (byte) data;
        break;

      case 0xFF55:
        //start DMA 
        this.cppu.setDMA((byte) data);
        break;

      case 0xFF4F:
        // System.out.println("setting VRAM bank: "+data);
        this.cppu.setVRAMBank(data);
        break;

      case 0xFF4D:
        System.out.println("speed switch requested: "+data);
        if((data & 0b00000001) != 0){
          this.timer.speedChangeRequested = true;
        }else{
          this.timer.speedChangeRequested = false;
        }
        break;

      }

      // if(position == 0xFF4A){
      // System.out.println("modifying lcdc: "+String.format("0x%02X",position)+",
      // data: "+data);
      // System.out.println("modifying WY: "+String.format("0x%02X",position)+", WY:
      // "+data);
      // }
      // this.gpu.setGPURegister(position, ((byte)data));

    } else if ((position >= 0xFE00) && (position < 0xFEA0)) {

      // this.gpu.write(position, (byte) data);
      this.cppu.write(position, (byte) data);

    } else if ((position >= 0xFEA0) && (position <= 0xFEFF)) {// NO IO

    } else if ((position >= 0x8000) && (position <= 0x9FFF)) { // tile area

      // this.gpu.write(position, (byte) data);
      this.cppu.write(position, (byte) data);

    } else if ((position >= 0xFF80) && (position < 0xFFFF)) {// this is for the stack, derp

      int index = position - 0xFF80;
      this.HRAM[index] = (byte) data;

    } else if ((position >= 0xC000) && (position < 0xD000)) { // internal RAM

      int RAMLocation = position - 0xC000;
      bankzero[RAMLocation] = (byte) data;
      // echoRAM[RAMLocation] = (byte) data;

    } else if ((position >= 0xD000) && (position <= 0xE000)) {// echo RAM

      int RAMLocation = position - 0xD000;
      // echoRAM[RAMLocation] = (byte) data;
      bankN[bankNumber][RAMLocation] = (byte) data;

    } else if ((position >= 0xA000) && (position <= 0xC000)) {

      this.cart.writeRAM(position, data);

    } else if ((position >= 0xFF00) && (position <= 0xFFFF)) { // special Registers

      if (position == 0xFF00) {
        joypad.setState((byte) data);
      } else {
        if (position == 0xFF07) {
          System.out.println("changing clock speed");
        }
        this.specialRegisters[position - 0xFF00] = (byte) data;
      }

    } else {
      System.out.println("can't write here: " + String.format("0x%02X", position));
    }
  }

  /*
   *
   * MMU READ
   *
   */

  public byte read(int pos) {
    int position = pos & 0xFFFF;

    if ((position >= 0xFF80) && (position <= 0xFFFE)) {// Stack, IE flag

      int index = position - 0xFF80;
      return this.HRAM[index];

    } else if (position == 0xFF00) {// requesting JOYPAD

      return this.joypad.getInput();

    } else if ((position >= 0x8000) && (position < 0xA000)) {// GPU AGAIN?
      return this.cppu.read(position);
      // return -1;
      // byte b = this.gpu.readFromVram(position);
      // return b;

    } else if ((position >= 0xC000) && (position < 0xD000)) {//BANK0

      return this.bankzero[position - 0xC000];

    } else if ((position >= 0xD000) && (position < 0xE000)) {// BANKN RAM

      try{
        return this.bankN[bankNumber][position - 0xD000];
      }catch(Exception e){
        System.out.println("failed to read: "+(position));
        System.exit(-1);
        return -1;
      }

    } else if ((position >= 0x0000) && (position < 0x4000)) {// ROM MEMORY

      return this.cart.getByte(position);

    } else if (position < 0x8000) {

      return this.cart.readBank(position); 

    } else if(position == 0xFF70){
      
      // System.out.println("bank read: "+bankNumber);
      return (byte) bankNumber;
      // return (this.timer.doubleSpeed)? (byte) 1 : 0;

    } else if ((position >= 0xFF40) && (position < 0xFF70)) {// GPU

      // byte result = this.gpu.readFromVram(position);
      // System.out.println("reading from gpu registers:
      // "+String.format("0x%02X",position)+" -> "+String.format("0x%02X",result));
      // return result;
      byte result = -1;

      switch (position) {

      case 0xFF4D:
        System.out.println("reading speed");
        result = (this.timer.doubleSpeed)? (byte) 0x80 : 0;
        // result = 1;
        break;  

      case 0xFF40:
        result = this.cppu.LCDC;
        break;

      case 0xFF41:
        result = this.cppu.STAT;
        break;

      case 0xFF42:
        result = this.cppu.SCY;
        break;

      case 0xFF43:
        result = this.cppu.SCX;
        break;

      case 0xFF44:
        result = this.cppu.LY;
        break;

      case 0xFF45:
        result = this.cppu.LYC;
        break;

      case 0xFF46:
        result = this.cppu.DMA;
        break;

      case 0xFF47:
        result = this.cppu.BGP;
        break;

      case 0xFF48:
        result = this.cppu.OBP0;
        break;

      case 0xFF49:
        result = this.cppu.OBP1;
        break;

      case 0xFF4A:
        result = this.cppu.WY;
        break;

      case 0xFF4B:
        result = this.cppu.WX;
        break;

      case 0xFF68:

        result = this.cppu.BGPI;
        break;

      case  0xFF69: //nice
        result = this.cppu.readBGP();
        break;

      case 0xFF6A:
        // System.out.println("reading OBJ");
        result = this.cppu.OBPI;
        break;

      case 0xFF6B:
        // System.out.println("reading 0xFF6B");
        result = this.cppu.readObjP();
        break;

      case 0xFF4F:
        result = (byte) this.cppu.readBank();
        // System.out.println("reading VRAM bank: "+result);
        break;
        
      default:
        result = -1;
        break;
      }
      // System.out.println("reading from gpu registers:
      // "+String.format("0x%02X",position)+" -> "+String.format("0x%02X",result));
      return result;

    } else if ((position >= 0xA000) && (position < 0xC000)) {

      return this.cart.readRAM(position);

    } else if ((position > 0xFF00) && (position <= 0xFFFF)) {// INTERRUPTS

      return this.specialRegisters[position - 0xFF00];

    } else {
      // System.out.println("bad address readz: "+position);
      return (byte) 0x00;
    }
  }

  public byte[] save() {
    ByteBuffer buffer = ByteBuffer.allocate(100_000);
    //write header
    //then length of data we are going to write
    //then all of the data

    String header = "MMU";
    String specregHeader = "SPR";
    String internalRAMHeader = "IRH";
    String echoRAMHeader = "ERM";
    String HRAMHeader = "HRH";

    appendBuffer(header, new byte[4], buffer);
    appendBuffer(specregHeader, this.specialRegisters, buffer);
    appendBuffer(internalRAMHeader, this.internalRAM, buffer);
    appendBuffer(echoRAMHeader, this.echoRAM, buffer);
    appendBuffer(HRAMHeader, this.HRAM, buffer);

    return buffer.array();
  }

  public void load(byte[] data){

    byte[] bytelength = new byte[4];
    ByteBuffer buffer = ByteBuffer.wrap(data);
    
    String head = "MMU";
    String specregHeader = "SPR";
    String internalRAMHeader = "IRH";
    String echoRAMHeader = "ERM";
    String HRAMHeader = "HRH";

    loadData(head, buffer, bytelength);
    loadData(specregHeader, buffer, this.specialRegisters);
    loadData(internalRAMHeader, buffer, this.internalRAM);
    loadData(echoRAMHeader, buffer, this.echoRAM);
    loadData(HRAMHeader, buffer, this.HRAM);

    // buffer.get(header); //MMU
    // length = buffer.getInt(); //0
    // buffer.get(bytelength);
    
    // buffer.get(header); //SPR
    // length = buffer.getInt(); //??
    // buffer.get(this.specialRegisters);

    // buffer.get(header); //IRH
    // length = buffer.getInt(); //??
    // buffer.get(this.internalRAM);

    // buffer.get(header); //ERM
    // length = buffer.getInt(); //??
    // buffer.get(this.echoRAM);

    // buffer.get(header); //HRH
    // length = buffer.getInt(); //??
    // buffer.get(this.HRAM);
  }
}
