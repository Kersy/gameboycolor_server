package com.laconic.gameboy;

import com.laconic.gameboy.PPU;

public final class ColorPPU extends PPU {

    //vram = 6144
    /* Gameboy Color*/
    protected boolean DO_DMA = false;

    //palettes
    byte BGPI = 0;
    byte BGPD = 0;
    byte OBPI = 0;
    byte OBPD = 0;
    
    //HDMA
    byte SRCH = 0;
    byte SRCL = 0;
    byte DSTH = 0;
    byte DSTL = 0;

    byte HDMA = 0;

    private byte[] BGPMemory = new byte[64];
    private byte[] OBJMemory = new byte[64];

    public ColorPPU(GBMMU mmu, GBInterruptManager ime){
        super(mmu,ime);
    }

    @Override
    public void cycle(int cycles){
        if(DO_DMA && this.mode == PPU_MODE.HBLANK){
            doDMA(16);
        }
        processCycles(cycles);
    }

    private int convertIndex(byte pointer){
        return ((pointer & 0b00111111) & 0xFF);
    }

    private boolean autoInc(byte pointer){
        return (pointer & 0x80) != 0;
    }

    public byte readObjP(){
        int index = convertIndex(OBPI);
        return this.OBJMemory[index];
    }

    public byte readBGP(){
        int index = convertIndex(BGPI);
        return this.BGPMemory[index];
    }

    public void writeOBJP(byte data){
        this.OBJMemory[convertIndex(OBPI)] = data;
        if(autoInc(OBPI)){
            this.OBPI++;
        }
    }   

    public void writeBGP(byte data){
        this.BGPMemory[convertIndex(BGPI)] = data;
        if(autoInc(BGPI)){
            this.BGPI++;
        }
    }

    private void doDMA(int len){
        System.out.println("doing cycles");
        short raddress = (short)(((this.SRCH << 8) + (this.SRCL)) & 0xFFF0);
        short waddress = (short)(((this.DSTH << 8) + (this.DSTL)) & 0x1FF0);
        for(int i=0; i<len-1; i++){//may not want to subtract 1
            write(waddress+i,this.mmu.read(raddress+i));
            this.SRCH++;
            this.SRCL++;
            this.DSTH++;
            this.DSTL++;
            this.HDMA--;
        }
    }

    public void setDMA(byte dma){
        this.HDMA = dma;
        if((this.HDMA & 0x80) == 0){//doing a general DMA, otherwise an H-blank DMA
            byte len = (byte)((this.HDMA & 0x7F) * 0x10);
            doDMA(len);
            this.DO_DMA = false;
        }else{
            this.DO_DMA = true;
        }
    }

    public void setVRAMBank(int bankNum){
        this.VRAM_BANK = (bankNum == 0)? 0 : 1;
    }

    public int readBank(){
        return this.VRAM_BANK;
    }

    @Override
    protected void drawLine(){
        if(displayOn()){
            // System.out.println("sprite size: "+spriteSize());
            int wy = (this.WY & 0xFF);
            int wx = (this.WX & 0xFF) - 7;
            int sy = this.SCY & 0xFF;
            int sx = this.SCX & 0xFF;

            int tileTableNumber = (bgWinTileTableSelect()? 0 : 1);
            byte[] tileMap = bgTileMapSelect()? tileMapTwo[0] : tileMapOne[0];
            byte[] attr = (bgTileMapSelect() ? tileMapTwo[1] : tileMapOne[1]);

            int lineNumber = ((this.scanline + sy) % 8);
            int tileRow = (((this.scanline + sy) / 8) % 32);

            if(windowOn() && scanline >= wy) {
                lineNumber = (this.scanline - wy) % 8;
                tileRow = ((this.scanline - wy) / 8) % 32;
                byte[] winTileMap = (this.winTileMapSelect() ? tileMapTwo[0] : tileMapOne[0]);
                byte[] attributes = (this.winTileMapSelect() ? tileMapTwo[1] : tileMapOne[1]);

                int windowTileStart = ((tileRow * 32) + (wx / 8)) % 32;
                for (int i = windowTileStart; i < 20; i++) {
                    int tileNum = (tileRow * 32) + ((i + (wx / 8)) % 32);
                    Tile tile = getTileWithAttributes(winTileMap[tileNum], tileTableNumber, attributes[tileNum], false);
                    byte[][] pixels = tile.convertRawColor()[lineNumber];
                    drawRow(pixels, i);
                }
                if(windowTileStart > 0){
                    for(int i=0; i<windowTileStart; i++){
                        int tileNum = (tileRow * 32)+((i+(wx / 8)) % 32);
                        Tile tile = getTileWithAttributes(tileMap[tileNum],tileTableNumber,attributes[tileNum], false);
                        byte[][] pixels = tile.convertRawColor()[lineNumber];
                        drawRow(pixels,i);
                    }
                }
            }else{
                for(int i=0; i<20; i++){//need 20 tiles to fill 160pixels
                    int tileNum = (tileRow * 32)+((i+(sx / 8)) % 32);
                    Tile tile = getTileWithAttributes(tileMap[tileNum],tileTableNumber,attr[tileNum], false);
                    byte[][] pixels = tile.convertRawColor()[lineNumber];
                    drawRow(pixels,i);
                }
            }
        }
    }

    public byte[][][] getTileTableN(int mapNumber, int tableNumber, int bankNumber){
        byte[] table = (mapNumber == 0)? this.tileMapOne[0] : this.tileMapTwo[0];
        byte[] attributes = (mapNumber == 0)? this.tileMapOne[1] : this.tileMapTwo[1];
        byte[][][] scrn = new byte[256][256][2];
        for(int i=0; i<1024; i++){  
            int tileRow = (i / 32);
            int tileCol = (i % 32);
            Tile tile = getTileWithAttributes(this.tileMapOne[0][i], tableNumber, this.tileMapOne[1][i], bankNumber);
            byte[][][] data = tile.convertRawColor();
            for(int j=0; j<8; j++){
                for(int z=0; z<8; z++){
                    scrn[(tileRow * 8)+j][(tileCol * 8)+z] = data[j][z];
                }
            }
        }
        return scrn;
    }

    protected void drawRow(byte[][] row, int i){
        for(int j=0; j<8; j++) {
            display[this.scanline][(8 * i + j)][0] = row[j][0];
            display[this.scanline][(8 * i + j)][1] = row[j][1];
            // display[this.scanline][(8 * i + j)][1] = row[j][1];
        }
    }

    private Tile getTileWithAttributes(int tileNumber, int table, byte attr, boolean sprite){
        int palNumber = (attr & 0b00000111);
        int bankNumber = (attr & 0b00001000);
        boolean flipX = ((attr & 0b00100000) != 0); //TODO
        boolean flipY = ((attr & 0b01000000) != 0); //TODO
        boolean priority = ((attr & 0b10000000) != 0); //TODO
        if(sprite){
            return getTile(tileNumber, table, getPalette(palNumber,this.OBJMemory), bankNumber);            
        }else{
            return getTile(tileNumber, table, getPalette(palNumber,this.BGPMemory), bankNumber);
        }
    }

    private Tile getTileWithAttributes(int tileNumber, int table, byte attr, int bank){
        int palNumber = (attr & 0b00000111);
        int bankNumber = (attr & 0b00001000);
        boolean flipX = ((attr & 0b00100000) != 0); //TODO
        boolean flipY = ((attr & 0b01000000) != 0); //TODO
        boolean priority = ((attr & 0b10000000) != 0); //TODO
        return getTile(tileNumber, table, getPalette(palNumber,this.BGPMemory), bank);
    }

    public byte[] getPalette(int paletteNumber, byte[] palMemory){
        byte[] temp = new byte[8];
        int index = 8 * paletteNumber;
        // if(index > (8 * 5)){ System.out.println("high index: "+(index/8)); }
        for(int i=0; i<temp.length; i++){
            temp[i] = palMemory[i+index];
        }
        return temp;
    }

    public Tile getTile(int tileNumber, int table, byte[] b, int bank){
        byte[] tile = new byte[16];
        byte[] tileMap = new byte[1024];
        try{
        switch(table){
            case 0: tileMap = this.tileTableOne[bank];
            break;

            case 1: tileMap = this.tileTableTwo[bank];
            break;
        }
    }   catch(Exception e){
            System.out.println("bank is: "+bank);
        }
        if(tileNumber < 0){
          tileMap = this.tileTableOne[bank];
        }
        for(int i=0; i<16; i++){
          tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
        }
        return new Tile(tile,b);
    }

    public void loadSpritesToDisplay(){
        byte bgp = BGP;
        byte paletteZero = (byte)(bgp & 3);

        for(int i=40-1; i>=0; i--){
            int y = (OAM[i * 4] & 0xFF) - 16; //y pos of sprite
            int x = (OAM[i * 4 + 1] & 0xFF) - 8; //x pos of sprite
            int tile = (OAM[i * 4 + 2] & 0xFF); //tile to use for sprite
            int attr = OAM[i * 4 + 3]; //attributes of sprite

            boolean flipX = ((attr & 0b00100000) != 0); //ignoring for now
            boolean flipY = ((attr & 0b01000000) != 0); //ignoring for now
            boolean behindBG = ((attr & 0b10000000) == 0); //I guess not paint it if it's behind?
            int bank = (((attr & 0b00001000) != 0)? 1 : 0); //ignoring for now E4 then C4
            int pal = (attr & 0b00000111);

            Sprite s;
            if(spriteSize()){
                if(!((x < 0 || x >= 160) || (y < 0 || y >= 144))){
                    s = getExtendedSprite(tile,0,getPalette(pal,this.OBJMemory),bank);
                    byte[][][] sprite = s.convertRawColor();
                    sprite = (flipX)? s.flipX(sprite) : sprite;
                    sprite = (flipY)? s.flipY(sprite) : sprite;
                    for(int z=0; z<8; z++){
                        for(int j=0; j<16; j++){
                            byte pixel = sprite[j][z][0];
                            byte p2 = sprite[j][z][1];
                                if(behindBG){
                                    if(display[(j+y)%144][(z+x)%160][0] == paletteZero || pixel != 4){
                                        display[(j+y)%144][(z+x)%160][0] = pixel;
                                        display[(j+y)%144][(z+x)%160][1] = p2;
                                    }
                                }else{
                                    if(pixel != 4 && p2 != 4){
                                        display[(j+y)%144][(z+x)%160][0] = pixel;
                                        display[(j+y)%144][(z+x)%160][1] = p2;
                                    }
                                }
                            }
                        }
                    }
            }else{
                s = getSprite(tile,0,getPalette(pal,this.OBJMemory),bank);
                if(!((x < 0 || x >= 160) || (y < 0 || y >= 144))){
                    byte[][][] sprite = (flipX)? s.flipX(s.convertRawColor()) : s.convertRawColor();
                    sprite = (flipY)? s.flipY(sprite) : sprite;
                    for(int z=0; z<8; z++){
                        for(int j=0; j<8; j++){
                            byte pixel = sprite[j][z][0];
                            byte p2 = sprite[j][z][1];
                            if(behindBG){
                                if(display[(j+y)%144][(z+x)%160][0] == paletteZero || pixel != 4){
                                    display[(j+y)%144][(z+x)%160][0] = pixel;
                                    display[(j+y)%144][(z+x)%160][1] = p2;
                                }
                            }else{
                                if(pixel != 4 && p2 != 4){
                                    display[(j+y)%144][(z+x)%160][0] = pixel;
                                    display[(j+y)%144][(z+x)%160][1] = p2;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    final public Sprite getSprite(int tileNumber, int table, byte[] pal, int bank){
        byte[] tile = new byte[16];
        byte[] tileMap = new byte[1024];
        switch(table){
            case 0:
                tileMap = this.tileTableOne[bank];
                break;
            case 1:
                tileMap = this.tileTableTwo[bank];
                break;
        }
        if(tileNumber < 0){
            tileMap = this.tileTableOne[bank];
        }
        for(int i=0; i<16; i++){
            tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
        }
        return new Sprite(tile,pal);
    }

    final public Sprite getExtendedSprite(int tileNumber, int table, byte[] pal, int bank){
        byte[] tile = new byte[32];
        byte[] tileMap = new byte[1024];
        switch(table){
            case 0:
                tileMap = this.tileTableOne[bank];
                break;
            case 1:
                tileMap = this.tileTableTwo[bank];
                break;
        }
        if(tileNumber < 0){
            tileMap = this.tileTableOne[bank];
        }
        for(int i=0; i<32; i++){
            tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
        }
        return new Sprite(tile,pal);
    }
}