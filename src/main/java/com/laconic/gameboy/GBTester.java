package com.laconic.gameboy;

import java.io.FileInputStream;
import java.io.File;
import java.util.*;
import java.nio.file.Files;
// import static org.junit.Assert.assertEquals;

public class GBTester{

  public static void main(String args[]){

//    GBGPU gpu = new GBGPU();
//    GBCart cart = new GBCart("/home/matthew/chip8/gameboy/ROMS/tetris.gb");
//    GBMMU mmu = new GBMMU(gpu,cart);
//    GBProcessor cpu = new GBProcessor(mmu);
//    GBClock clock = new GBClock(cpu,gpu);
//    GBRegisters registers = new GBRegisters();
//
//    mmu.write(0x8000,(byte)0x04);
//    mmu.write(0x8001,(byte)0x04);
//    mmu.write(0x8002,(byte)0x04);
//    mmu.write(0x8003,(byte)0x04);
//    mmu.write(0x8004,(byte)0x04);
//    mmu.write(0x8005,(byte)0x04);
//    mmu.write(0x8006,(byte)0x04);
//    mmu.write(0x8007,(byte)0x04);
//    mmu.write(0x8009,(byte)0x04);
//    mmu.write(0x800A,(byte)0x04);
//
//    int val = 160;
//    System.out.println(160 & 128);
//    System.out.println(160 & 64);
//    System.out.println(160 & 32);
//    System.out.println(160 & 16);

    byte b = 1;
    int result = 0;
    result = (b - 1) & 0xFF;
    System.out.println(result);

    // int num=0;
    // Tile[] tiles = gpu.getTileTableOne();
    // for(Tile t : tiles){
    //   byte[][] sprite = t.convertRawData();
    //   for(int i=0; i<sprite.length; i++){
    //     for(int j=0; j<sprite[i].length; j++){
    //       // display[i][j] = sprite[j][i];
    //       System.out.print(sprite[j][i]+" ");
    //     }
    //     System.out.println();
    //   }
    //   num++;
    // }



    // registers.writeHL(0xFF01);
    // System.out.println(registers.getHL() & 0xFFFF);
    // registers.decHL();
    // System.out.println(registers.getHL() & 0xFFFF);
    // registers.incHL();
    // System.out.println(registers.getHL() & 0xFFFF);

    // byte val = (byte) 0xFB;
    // System.out.println(val);
    }

    public static void printPosition(int tileNumber){ //this gives us the tile number, now we need to translate that to the actual background position
      int row = tileNumber % 32;
      int column = tileNumber / 32;
      System.out.println("x: "+column+", y: "+row);
      int[][] data = new int[8][8];
      printActualPosition(row,column,data);
    }

    public static void printActualPosition(int row, int column, int[][] sprite){
      int r = row * 8;
      int c = column * 8;
      System.out.println("true x: "+r+", true y: "+c);
      int x = 0;
      int y = 0;
      for(int i=0; i<8; i++){
        for(int j=0; j<8; j++){
          System.out.print(sprite[i][j]+" ");
          y = c+j;
        }
        x = r+i;
        System.out.println();
      }
      System.out.println("final x: "+x+" final y: "+y);
    }

}
