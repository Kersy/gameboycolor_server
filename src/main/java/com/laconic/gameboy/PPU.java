package com.laconic.gameboy;

import java.nio.ByteBuffer;

import com.laconic.emulator.StatefulComponent;

public class PPU implements StatefulComponent {

    protected int VRAM_BANK = 0;

    byte LCDC = 0;
    byte STAT = 0;
    byte SCX = 0;
    byte SCY = 0;
    byte LY = 0;
    byte LYC = 0;
    byte DMA = 0;
    byte BGP = 0;
    byte OBP0 = 0;
    byte OBP1 = 0;
    byte WY = 0;
    byte WX = 0;
    
    protected byte[] OAM = new byte[160]; //0xFE00 - 0xFE9F
    protected byte[][] tileTableOne = new byte[2][4096];
    protected byte[][] tileTableTwo = new byte[2][4096];
    protected byte[][] tileMapOne = new byte[2][1024]; //0x9800 - 0x9BFF
    protected byte[][] tileMapTwo = new byte[2][1024]; //0x9C00 - 0x9FFF

    protected byte[][][] spriteBuffer = new byte[144][160][2];
    protected byte[][][] display = new byte[144][160][2];

    protected GBInterruptManager ime;
    public PPU_MODE mode;

    public int scanline = 0;
    protected int totalClocks = 0;

    protected GBMMU mmu;

    public enum PPU_MODE{
        VBLANK, HBLANK, OAM, TRANS;
    }

    public PPU(GBMMU mmu, GBInterruptManager ime){
        this.ime = ime;
        this.mmu = mmu;
        this.mode = PPU_MODE.OAM; //I guess?
    }

    public PPU(){
        this.mode = PPU_MODE.OAM; //I guess?
    }

    protected void updateSTAT(){
        byte current = (byte)(this.STAT & 0xFC);
        switch(this.mode){
            case TRANS: this.STAT = (byte)(current | 3); break;
            case OAM: this.STAT = (byte)(current | 2); break;
            case VBLANK: this.STAT = (byte)(current | 1); break;
            case HBLANK: this.STAT = current; break;
        }
    }

    public void cycle(int cycles){
        processCycles(cycles);
    }

    protected void processCycles(int cycles){
        // PPU_MODE oldMode = this.mode;
        totalClocks += (cycles / 4);
        
        if(LYC == LY){
            this.ime.requestInterrupt(GBInterruptManager.InterruptType.LCDC);
            this.STAT = ((byte)(STAT | 0b100));
        }else{
            this.STAT = ((byte)(STAT ^ 0b100));
        }

        if(scanline >= 144){
            if(mode != PPU_MODE.VBLANK){
                mode = PPU_MODE.VBLANK;
                this.ime.requestInterrupt(GBInterruptManager.InterruptType.VBLANK);
                STAT = (byte)((STAT & ~3) | 0x01);
            }
            if(totalClocks >= 114){
                totalClocks = 0;
                scanline += 1;
                LY = (byte)scanline;
            }
            if(scanline > 153){
                scanline = 0;
                LY = 0;
                totalClocks = 0;
            }
        }else{
            if(totalClocks < 20){
                STAT = (byte)((STAT & ~3) | 0x02);
                mode = PPU_MODE.OAM;
            }else if(totalClocks < 63){
                STAT = (byte)((STAT & ~3) | 0x03);
                mode = PPU_MODE.TRANS; 
            }else if(totalClocks < 114){
                STAT = (byte)((STAT & ~3) | 0x00);
                mode = PPU_MODE.HBLANK;
            }else if(totalClocks >= 114){
                STAT = (byte)((STAT & ~3) | 0x02);
                mode = PPU_MODE.OAM;

                drawLine();
                drawSprites();
                scanline += 1;
                LY = (byte)scanline;
                totalClocks = 0;
            }
        }
    }

    public byte[][][] getSpriteBuffer(){
        return this.spriteBuffer;
    }

    public byte[][][] getBackgroundBuffer() {
        return this.display;
    }

    protected void drawLine(){
        if(displayOn()){
            int wy = (this.WY & 0xFF);
            int wx = (this.WX & 0xFF) - 7;
            int sy = this.SCY & 0xFF;
            int sx = this.SCX & 0xFF;

            int tileTableNumber = (bgWinTileTableSelect()? 0 : 1);
            byte[] tileMap = bgTileMapSelect()? tileMapTwo[VRAM_BANK] : tileMapOne[VRAM_BANK];

            int lineNumber = ((this.scanline + sy) % 8);
            int tileRow = (((this.scanline + sy) / 8) % 32);

            if(windowOn() && scanline >= wy) {
                lineNumber = (this.scanline - wy) % 8;
                tileRow = ((this.scanline - wy) / 8) % 32;
                byte[] winTileMap = (this.winTileMapSelect() ? tileMapTwo[VRAM_BANK] : tileMapOne[VRAM_BANK]);
                byte[] attributes = (this.winTileMapSelect() ? tileMapOne[VRAM_BANK] : tileMapTwo[VRAM_BANK]);

                int windowTileStart = ((tileRow * 32) + (wx / 8)) % 32;
                for (int i = windowTileStart; i < 20; i++) {
                    int tileNum = (tileRow * 32) + ((i + (wx / 8)) % 32);
                    Tile tile = getTile(winTileMap[tileNum], tileTableNumber, BGP,0);
                    byte[] pixels = tile.convertRawData()[lineNumber];
                    drawRow(pixels, i);
                }
                if(windowTileStart > 0){
                    for(int i=0; i<windowTileStart; i++){
                        int tileNum = (tileRow * 32)+((i+(wx / 8)) % 32);
                        Tile tile = getTile(tileMap[tileNum],tileTableNumber,BGP,0);
                        byte[] pixels = tile.convertRawData()[lineNumber];
                        drawRow(pixels,i);
                    }
                }
            }else{
                for(int i=0; i<20; i++){//need 20 tiles to fill 160pixels
                    int tileNum = (tileRow * 32)+((i+(sx / 8)) % 32);
                    try{
                        Tile tile = getTile(tileMap[tileNum],tileTableNumber,BGP,0);
                        byte[] pixels = tile.convertRawData()[lineNumber];
                        drawRow(pixels,i);
                    }catch(Exception e){
                        System.out.println("tileNumber: "+tileNum+", scanline: "+this.scanline);
                    }
                }
            }
        }
    }

    protected void drawRow(byte[] row, int i){
        for(int j=0; j<8; j++) {
            display[this.scanline][(8 * i + j)][0] = row[j];
            display[this.scanline][(8 * i + j)][1] = row[j];
        }
    }

    public Tile getTile(int tileNumber, int table, byte b, int bank){
        byte[] tile = new byte[16];
        byte[] tileMap = new byte[1024];
        switch(table){
          case 0: tileMap = this.tileTableOne[bank];
          break;

          case 1: tileMap = this.tileTableTwo[bank];
          break;
        }
        if(tileNumber < 0){
          tileMap = this.tileTableOne[bank];
        }
        for(int i=0; i<16; i++){
          tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
        }
        byte[] bytes = {(byte)32, (byte)12, (byte)101, (byte)3, (byte)-122, (byte)55, (byte)22, (byte)16};
        return new Tile(tile,bytes);
    }

    protected void drawSprites(){
        loadSpritesToDisplay();
    }

    public void loadSpritesToDisplay(){
        byte bgp = BGP;
        byte paletteZero = (byte)(bgp & 3);

        for(int i=0; i<40; i++){
            int y = (OAM[i * 4] & 0xFF) - 16; //y pos of sprite
            int x = (OAM[i * 4 + 1] & 0xFF) - 8; //x pos of sprite
            int tile = (OAM[i * 4 + 2] & 0xFF); //tile to use for sprite
            int attr = OAM[i * 4 + 3]; //attributes of sprite
            boolean flipX = ((attr & 0b00100000) != 0); //ignoring for now
            boolean flipY = ((attr & 0b01000000) != 0); //ignoring for now
            boolean behindBG = ((attr & 0b10000000) == 0); //I guess not paint it if it's behind?
            byte objp = (((attr & 0b00010000) != 0)? OBP1: OBP0); //ignoring for now E4 then C4

            Sprite s = getSprite(tile,0,objp);
            if((x < 0 || x >= 160) || (y < 0 || y >= 144)){
                //don't draw
            }else{
                byte[][][] sprite = (flipX)? s.flipX(s.convertRawColor()) : s.convertRawColor();
                sprite = (flipY)? s.flipY(sprite) : sprite;
                for(int z=0; z<8; z++){
                    for(int j=0; j<8; j++){
                        byte pixel = sprite[j][z][0];
                        byte p2 = sprite[j][z][1];
                        if(behindBG){
//                            if(backgroundBuffer[(j+y)%144][(z+x)%160] == paletteZero && pixel != 4){
                            if(display[(j+y)%144][(z+x)%160][0] == paletteZero && pixel != 4){
                                display[(j+y)%144][(z+x)%160][0] = pixel;
                                display[(j+y)%144][(z+x)%160][1] = p2;
                            }
                        }else{
                            if(pixel != 4){
                                display[(j+y)%144][(z+x)%160][0] = pixel;
                                display[(j+y)%144][(z+x)%160][0] = p2;
                            }
                        }
                    }
                }
            }
        }
    }

    // protected void loadWindowToDisplay(){
    //     Tile[] tiles = getWindowTable();

    //     int wy = this.WY;
    //     int wx = this.WX;
    //     int num = 0;
    //     for(Tile t : tiles){
    //         int row = (num / 32) * 8; //row start
    //         int column = (num % 32) * 8; //column start
    //         num++;
    //         for(int i=0; i<8; i++){
    //             for(int j=0; j<8; j++){
    //                 int x = (j + column - 7 + wx);
    //                 int y = (i + row + wy);
    //                 if((x >= 0 && x < 160) && (y >= 0 && y < 144)){
    //                     this.display[y][x] = t.convertRawData()[i][j];
    //                 }
    //             }
    //         }
    //     }
    // }

    public Sprite getSprite(int tileNumber, int table,byte b){
        byte[] tile = new byte[16];
        byte[] tileMap = new byte[1024];
        switch(table){
            case 0:
                tileMap = this.tileTableOne[VRAM_BANK];
                break;
            case 1:
                tileMap = this.tileTableTwo[VRAM_BANK];
                break;
        }
        if(tileNumber < 0){
            tileMap = this.tileTableOne[VRAM_BANK];
        }
        for(int i=0; i<16; i++){
            tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
        }
        byte[] bytes = {(byte)32, (byte)12, (byte)101, (byte)3, (byte)-122, (byte)55, (byte)22, (byte)16};
        return new Sprite(tile,bytes);
    }

    public Tile[] getWindowTable(){
        int tableNumber = (this.bgWinTileTableSelect()? 0 : 1);
        Tile[] bgTiles = new Tile[1024];
        int num = 0;
        byte[] bgmap;
        if(winTileMapSelect()){
            bgmap = tileMapTwo[VRAM_BANK];
        }else{
            bgmap = tileMapOne[VRAM_BANK];
        }
        for(byte b : bgmap){
            bgTiles[num] = getTile(b,tableNumber,BGP,0);
            num++;
        }
        return bgTiles;
    }

    public void write(int address, byte data){
        if((address >= 0xFE00) && (address < 0xFEA0)){
            this.OAM[address - 0xFE00] = data;
        }else if((address >= 0x8000) && (address <= 0x8FFF)){
            this.tileTableOne[VRAM_BANK][address - 0x8000] = data;
        }else if(address <= 0x97FF){
            this.tileTableTwo[VRAM_BANK][(address - 0x9000)] = data;
        }else if ((address >= 0x9800) && (address < 0x9C00)){
            this.tileMapOne[VRAM_BANK][address - 0x9800] = data;
        }else if ((address >= 0x9C00) && (address < 0xA000)){
            this.tileMapTwo[VRAM_BANK][address - 0x9C00] = data;
        }else{
            System.out.println("GPU - attempting to write outside of VRAM: "+address);
        }
    }

    public byte read(int address) {
        if((address >= 0xFE00) && (address < 0xFEA0)){
            return (this.OAM[address - 0xFE00]);
        }else if((address >= 0x8000) && (address <= 0x8FFF)){
            return this.tileTableOne[VRAM_BANK][address - 0x8000];
        }else if(address <= 0x97FF){
            return this.tileTableTwo[VRAM_BANK][(address - 0x8FFF) + 2047];
        }else if ((address < 0x9C00)){
            return this.tileMapOne[VRAM_BANK][address - 0x9800];
        }else if ((address < 0xA000)){
            return this.tileMapTwo[VRAM_BANK][address - 0x9C00];
        }else{
            System.out.println("GPU - attempting to read outside of VRAM: "+address);
            return (byte)0xFF;
        }
    }

    protected boolean checkBit(int bit){ return (((LCDC & 0xFF) & (1 << bit)) != 0); }

    public boolean displayOn(){ return checkBit(7); }

    public boolean winTileMapSelect(){ return checkBit(6); }

    public boolean windowOn(){ return checkBit(5); }

    public boolean bgWinTileTableSelect(){ return checkBit(4); }

    public boolean bgTileMapSelect(){ return checkBit(3); }

    public boolean spriteSize(){ return checkBit(2); }

    public boolean spriteDisplayOn(){ return checkBit(1); }

    public boolean bgAndWindowDisplay(){ return checkBit(0); }

    @Override
    public byte[] save() {
        ByteBuffer buffer = ByteBuffer.allocate(100_000);
        //DON'T CARE ABOUT DMA, I DON'T THINK
        String PPU = "PPU";
        String LCD = "LCD";
        String STA = "STA";
        String SCX ="SCX";
        String SCY= "SCY";
        String SLY = "SLY";
        String LYC = "LYC";
        String BGP = "BGP";
        String OJ1 = "OJ1";
        String OJ2 = "OJ2";
        String SWY ="SWY";
        String SWX = "SWX";
        String OM = "OAM";
        String TT1 = "TTO";
        String TT2 = "TTT";
        String TM1 = "TMO";
        String TM2 = "TMT";

        String MODE = "MOD";
        String SCN = "SCN";
        String CLK = "CLK";
        
        appendBuffer(PPU, new byte[4], buffer);
        appendByte(LCD, this.LCDC, buffer);
        appendByte(STA, this.STAT, buffer);
        appendByte(SCX, this.SCX, buffer);
        appendByte(SCY, this.SCY, buffer);
        appendByte(SLY, this.LY, buffer);
        appendByte(LYC, this.LYC, buffer);
        appendByte(BGP, this.BGP, buffer);
        appendByte(OJ1, this.OBP0, buffer);
        appendByte(OJ2, this.OBP1, buffer);
        appendByte(SWY, this.WY, buffer);
        appendByte(SWX, this.WX, buffer);
        appendBuffer(OM, this.OAM, buffer);
        appendBuffer(TT1, this.tileTableOne[VRAM_BANK], buffer);
        appendBuffer(TT2, this.tileTableTwo[VRAM_BANK], buffer);
        appendBuffer(TM1, this.tileMapOne[VRAM_BANK], buffer);
        appendBuffer(TM2, this.tileMapTwo[VRAM_BANK], buffer);
        appendInt(MODE, this.mode.ordinal(), buffer);
        appendInt(SCN, this.scanline, buffer);
        appendInt(CLK, this.scanline, buffer);
    
        //don't think I actually need to save displaybuffer/spritebuffer
        return buffer.array();
    }

    @Override
    public void load(byte[] data) {
        // ByteBuffer buffer = ByteBuffer.allocate(100_000);
        ByteBuffer buffer = ByteBuffer.wrap(data);

        String PPU = "PPU";
        String LCD = "LCD";
        String STA = "STA";
        String SCX ="SCX";
        String SCY= "SCY";
        String SLY = "SLY";
        String LYC = "LYC";
        String BGP = "BGP";
        String OJ1 = "OJ1";
        String OJ2 = "OJ2";
        String SWY ="SWY";
        String SWX = "SWX";
        String OM = "OAM";
        String TT1 = "TTO";
        String TT2 = "TTT";
        String TM1 = "TMO";
        String TM2 = "TMT";

        String MODE = "MOD";
        String SCN = "SCN";
        String CLK = "CLK";

        loadData(PPU, buffer, new byte[4]);
        this.LCDC = loadByte(LCD, buffer);
        this.STAT = loadByte(STA, buffer);
        this.SCX = loadByte(SCX, buffer);
        this.SCY = loadByte(SCY, buffer);
        this.LY = loadByte(SLY, buffer);
        this.LYC = loadByte(LYC, buffer);
        this.BGP = loadByte(BGP, buffer);
        this.OBP0 = loadByte(OJ1, buffer);
        this.OBP1 = loadByte(OJ2, buffer);
        this.WY = loadByte(SWY, buffer);
        this.WX = loadByte(SWX, buffer);
        loadData(OM, buffer, this.OAM);
        loadData(TT1, buffer, this.tileTableOne[VRAM_BANK]);
        loadData(TT2, buffer, this.tileTableTwo[VRAM_BANK]);
        loadData(TM1, buffer, this.tileMapOne[VRAM_BANK]);
        loadData(TM2, buffer, this.tileMapTwo[VRAM_BANK]);
        this.mode = PPU_MODE.values()[loadInt(MODE, buffer)];
        this.scanline = loadInt(SCN, buffer);
        this.scanline = loadInt(CLK, buffer);
    }

}
