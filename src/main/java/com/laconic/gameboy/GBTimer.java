package com.laconic.gameboy;

import java.nio.ByteBuffer;

import com.laconic.emulator.StatefulComponent;

public class GBTimer implements StatefulComponent{

  private final static int CLOCK_SPEED = (4_194_304 / 4);

  private final static int TIMER_ONE = 4096;
  private final static int TIMER_TWO = 16780;
  private final static int TIMER_THREE = 65536;
  private final static int TIMER_FOUR = 262144;

  // private final static byte timerRequest = 0x04;

  //private final static int DIV_FRQ = CLOCK_SPEED/16384; //256

  private int cycleFreq = TIMER_ONE; //always start with this?
  private int timerCounter = CLOCK_SPEED/cycleFreq;
  private int MAXCYCLES = 69905;
  public int currentCycles = 0;
  public boolean doubleSpeed = false;
  public boolean speedChangeRequested = false;

  private GBMMU mmu;
  GBInterruptManager ime;

  public GBTimer(GBMMU mmu, GBInterruptManager interruptManager){
    this.mmu = mmu;
    this.ime = interruptManager;
  }

  public void switchDoubleSpeed(){
    this.doubleSpeed = true;
  }

  public void cycle(int numberOfCycles){
  //  this.currentCycles += numberOfCycles;
    this.currentCycles = numberOfCycles;

    if(getFreq() != this.cycleFreq){
      System.out.println("Frequency change "+getFreq());
      this.cycleFreq = getFreq();
      resetClock();
    }

    if(timerOn()){
      this.timerCounter -= numberOfCycles;

      if(timerCounter <= 0){
        resetClock();
        byte counter = (byte)(this.mmu.read(0xFF05) + 1); //where to restart the TMA register to
        this.mmu.write(0xFF05,counter); 

        if(counter == 0){ //it overflowed
          this.mmu.write(this.mmu.read(0xFF05),this.mmu.read(0xFF06));
          sendIR();
        }
      }
    }
  }

  public boolean updateScreen(){
    if(currentCycles > (MAXCYCLES * 5)){ //this reduces flickering significantly
      currentCycles = 0;
      return true;
    }else{
      return false;
    }
  }

  public boolean isStopped(){ return false; }

  public void setFreq(byte n){
    this.mmu.write(0xFF07, n);
  }

  public byte readFreq(){ //TODO: SET BACK TO PRIVATE I THINK
    return (byte)(this.mmu.read(0xFF07) & 0b011);
  }

  private int getFreq(){
    switch(readFreq()){

      case 0b00: return TIMER_ONE;

      case 0b01: return TIMER_FOUR;

      case 0b10: return TIMER_THREE;

      case 0b11: return TIMER_TWO;
    }
    return -1;
  }

  public void switchFreq(){
    //TODO: FIX THIS, WE COULD JUST MAKE N=64, 16, 4...
    byte freq = readFreq();
    int n = 0;
    switch(freq & 0xFF){

      case 0b00: n = 0;

      case 0b01: n = 3;

      case 0b10: n = 2;

      case 0b11: n = 1;
    }
    this.cycleFreq = TIMER_ONE * (4^n);
    if(doubleSpeed){
      this.timerCounter = (CLOCK_SPEED * 2)/this.cycleFreq;
    }else{
      this.timerCounter = CLOCK_SPEED/this.cycleFreq;
    }
  }

  public void sendIR(){ //TODO: SWAP TO PRIVATE
    this.ime.requestInterrupt(GBInterruptManager.InterruptType.TIMER);
  }

  private void resetClock(){
    if(doubleSpeed){
      this.timerCounter = (CLOCK_SPEED * 2)/this.cycleFreq;
    }else{
      this.timerCounter = CLOCK_SPEED/this.cycleFreq;
    }  }

  private boolean timerOn(){
    return ((this.mmu.read(0xFF07) & 0b100) != 0);
  }

  @Override
  public byte[] save() {
    ByteBuffer buffer = ByteBuffer.allocate(1_000);

    String header = "TMR";
    String cyclfrq = "CFR"; //always start with this?
    String counter = "CNT";
    String max = "MAX";
    String current = "CUR";

    appendBuffer(header, new byte[4], buffer);
    appendInt(cyclfrq, this.cycleFreq, buffer);
    appendInt(counter, this.timerCounter, buffer);
    appendInt(max, this.MAXCYCLES, buffer);
    appendInt(current, this.currentCycles, buffer);

    //tests
    // this.cycleFreq = 0;
    // this.timerCounter = 0;
    // this.MAXCYCLES = 0;
    // this.currentCycles = 0;

    return buffer.array();
  }

  @Override
  public void load(byte[] data) {
    ByteBuffer buffer = ByteBuffer.wrap(data);

    String header = "TMR";
    String cyclfrq = "CFR"; //always start with this?
    String counter = "CNT";
    String max = "MAX";
    String current = "CUR";

    loadData(header, buffer, new byte[4]);
    this.cycleFreq = loadInt(cyclfrq, buffer);
    this.timerCounter = loadInt(counter, buffer);
    this.MAXCYCLES = loadInt(max, buffer);
    this.currentCycles = loadInt(current, buffer);

  }




}
